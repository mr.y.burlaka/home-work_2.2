#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <atomic>
#include <condition_variable>
#include <windows.h> // ��� ������������� ������� Windows API ��� ��������� �����

using namespace std;

// ��������� ��� ��������� ����
const int ROAD_WIDTH = 20;    // ������ ������
const int ROAD_HEIGHT = 2;    // ������ ������ (���������� �����)
const char PLAYER_CHAR = '>'; // ������ ������
const char OBSTACLE_CHAR = '#'; // ������ �����������
const char EMPTY_CHAR = '=';    // ������ ������� �����
const int PLAYER_START_LANE = 0; // ��������� ������ ������

class Game 
{
public:
    // �����������, ���������������� ��������� ����
    Game() : gameOver(false), playerLane(PLAYER_START_LANE), score(0) 
    {
        road.resize(ROAD_HEIGHT, vector<char>(ROAD_WIDTH, EMPTY_CHAR)); // ������������� ������ ������� ���������
    }

    // ������� ������� ����
    void run() 
    {
        // ������ ������� ��� ��������� �����, ���������� � ���������� �������������
        thread inputThread(&Game::processInput, this);
        thread renderThread(&Game::render, this);
        thread obstaclesThread(&Game::processObstacles, this);

        // �������� ���������� ���� �������
        inputThread.join();
        renderThread.join();
        obstaclesThread.join();
    }

private:
    atomic<bool> gameOver;      // ���� ���������� ����
    atomic<int> playerLane;     // ������� ������ ������
    int score;                  // ���� ����
    vector<vector<char>> road;  // ������ ��� �������� ��������� ������
    mutex mtx;                  // ������� ��� ������������� ������� � ����� ������
    condition_variable cv;      // �������� ���������� ��� �������������

    // ������� ��������� ����� ������������
    void processInput() 
    {
        while (!gameOver) 
        {
            // �������� ��������� ������ W � S ��� ���������� �������
            if (GetAsyncKeyState('W') & 0x8000) 
            {
                unique_lock<mutex> lock(mtx); // ���������� ��������
                playerLane = max(playerLane - 1, 0); // ����������� ������ �����
                cv.notify_all(); // ����������� ���� ������ �������
            }
            if (GetAsyncKeyState('S') & 0x8000) 
            {
                unique_lock<mutex> lock(mtx); // ���������� ��������
                playerLane = min(playerLane + 1, ROAD_HEIGHT - 1); // ����������� ������ ����
                cv.notify_all(); // ����������� ���� ������ �������
            }
            this_thread::sleep_for(chrono::milliseconds(50)); // ��������� �������� ��� ���������� �������� �� ���������
        }
    }

    // ������� ���������� ����
    void render() 
    {
        while (!gameOver) 
        {
            this_thread::sleep_for(chrono::milliseconds(100)); // �������� ��� ���������� ����������

#ifdef _WIN32
            system("cls"); // ������� ������� �� Windows       
#else
            system("clear"); // ������� ������� �� ������ ����������
#endif

            unique_lock<mutex> lock(mtx); // ���������� ��������

            // ��������� ������ � ������
            for (int i = 0; i < ROAD_HEIGHT; ++i) 
            {
                for (int j = 0; j < ROAD_WIDTH; ++j) 
                {
                    if (i == playerLane && j == 1) 
                    {
                        cout << PLAYER_CHAR ; // ��������� ������
                    }
                    else 
                    {
                        cout << road[i][j]; // ��������� ������
                    }
                }
                cout << endl;
            }

            // ��������� �����
            cout << "Score: " << score << endl;
        }
    }

    // ������� ��������� �����������
    void processObstacles() 
    {
        while (!gameOver)
        {
            for (int i = 0; i < 2; ++i)
            {
                // �������� �������� ����������� � ����������� �� �������� score
                if (score >= 5 && score < 10)
                {
                    this_thread::sleep_for(chrono::milliseconds(350)); // �������� 350 �����������
                }
                else if (score >= 10)
                {
                    this_thread::sleep_for(chrono::milliseconds(250)); // �������� 250 �����������
                }
                else
                {
                    this_thread::sleep_for(chrono::milliseconds(500)); // �������� �� ��������� 500 �����������
                }

                unique_lock<mutex> lock(mtx); // ���������� ��������

                // ����������� ����������� �����
                for (int i = 0; i < ROAD_HEIGHT; ++i)
                {
                    for (int j = 0; j < ROAD_WIDTH - 1; ++j)
                    {
                        road[i][j] = road[i][j + 1]; // ����������� ����������� �� ���� ������� �����
                    }
                    road[i][ROAD_WIDTH - 1] = EMPTY_CHAR; // ������� ��������� �������
                }

                // ���������� ����� ������ ����������
                if ( road[0][1] == OBSTACLE_CHAR|| road[1][0] == OBSTACLE_CHAR)
                {
                    score++; 
                }

                // �������� ������������
                if (road[playerLane][1] == OBSTACLE_CHAR)
                {
                    gameOver = true; // ���������� ���� ��� ������������
                    cout << "Game Over! Final Score: " << score << endl; // ����� ��������� �����
                    return;
                }

            }
            
            // ��������� ���������� ����� �����������
            if (rand() % 2 == 0)
            {
                road[rand() % 2][ROAD_WIDTH - 1] = OBSTACLE_CHAR; // ���������� ����������� � ��������� �������    
            }

        }
    }
};

int main() {
    srand(static_cast<unsigned>(time(nullptr))); // ������������� ���������� ��������� �����
    Game game; // �������� ���������� ����
    game.run(); // ������ ����
    return 0;
}